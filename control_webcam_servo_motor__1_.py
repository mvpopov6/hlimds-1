SERVO_PIN = 27
LED_PIN = 14

import cv2
import RPi.GPIO as GPIO
from time import sleep
import numpy as np

GPIO.setmode(GPIO.BCM)
GPIO.setup(SERVO_PIN, GPIO.OUT)
GPIO.setup(LED_PIN, GPIO.OUT)

pwm = GPIO.PWM(SERVO_PIN, 50)
pwm.start(0)

def SetAngle(angle):
    duty = angle/18
    GPIO.output(SERVO_PIN, True)
    pwm.ChangeDutyCycle(duty)
    sleep(1)
    GPIO.output(SERVO_PIN, False)
    pwm.ChangeDutyCycle(0)
    
    
cam = cv2.VideoCapture(0)

low_red = np.array([161, 155, 84])
high_red = np.array([179, 255, 255])

    
while True:
    ret, img = cam.read()
    
    hsv_frame = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    red_mask = cv2.inRange(hsv_frame, low_red, high_red)
    
    moments = cv2.moments(red_mask, 1)
    
    dm01 = moments['m01']
    dm10 = moments['m10']
    dArea = moments['m00']
    
    if dArea > 100:
        x = int(dm10/dArea)
        y = int(dm01/dArea)
        cv2.circle(img, (x,y), 10, (0, 0, 255), -1)
        GPIO.output(LED_PIN, True)
        
        center_x = img.shape[1]/2
        SetAngle(min(max(90-(x-center_x), 0), 180))
        
    else:
        GPIO.output(LED_PIN, False)
        
    cv2.imshow('my_cam', img)
    
    if cv2.waitKey(10) == 27:
        break

cam.release()
cv2.destroyAllWindows()
GPIO.cleanup()